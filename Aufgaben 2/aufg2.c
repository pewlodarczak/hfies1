#include "aufg2.h"

int aNum = 20; // globale Variable

int main() {
    //aufg1();
    //aufg2();
    //aufg3();
    //aufg4();
    //aufg5();
    //aufg6();
    /*
    int num;
    printf("Geben Sie eine 2stellige Zahl ein:\n ");
    scanf("%d",&num);
    printf("%d ist umgekehrt: %d\n", num, aufg7(num));

    int num;
    printf("Geben Sie eine Zahl ein:\n ");
    scanf("%d",&num);
    int *val = &num;
    aufg8(val);
    printf("%d ist um eins erhöht\n", *val);

    int num;
    printf("Um wieviel soll %d erhöht werden?\n ", aNum);
    int tmp = aNum;
    scanf("%d",&num);
    aufg9(num);
    printf("%d ist um %d erhöht: %d\n", tmp, num, aNum);

    int str[20];
    printf("Geben Sie einen String ein:\n");
    scanf("%s", &str);
    printf("%s ist identisch wie \"Cprogramming\" %s \n", str, aufg10(&str) ? "wahr" : "falsch");

    int age;
    printf("Wie alt sind Sie?\n");
    scanf("%d", &age);
    aufg11(age);
    */
   aufg12(1);
}

void aufg1() {
    int a,cube;
    printf("Geben Sie die Kantzenlänge ein:\n ");
    scanf("%d",&a);
    cube = (a*a*a);
    printf("Volumen ist: %d\n",cube);
}

void aufg2() {
    int i,j;
    printf("Geben Sie 2 ganze Zahlen ein:\n ");
    scanf("%d %d",&i, &j);
    if(i == j) {
        printf("Die Zahlen sind identisch:\n ");
        return;
    } else {
        if(i > j) {
            printf("%d ist grösser als %d\n ", i, j);
            printf("Differenz %d\n ", i - j);
        } else {
            printf("%d ist grösser als %d\n ", j, i);
            printf("Differenz %d\n ", j - i);
        }

    }
}

void aufg3() {
    int i;
    printf("Geben Sie eine Zahl ein:\n ");
    scanf("%d",&i);
    if(i < 0) {
        printf("%d ist negativ\n ", i);
        return;
    } else if(i == 0) {
        printf("%d ist null\n ", i);
    } else {
        printf("%d ist positiv\n ", i);
    }
}

void aufg4() {
    char day[10];
    printf("Geben Sie einen Wochentag ein:\n ");
    scanf("%s",&day);
    if(strcmp(day, "Montag") != 0 && strcmp(day, "Dienstag") != 0 && strcmp(day, "Mittwoch") != 0 && strcmp(day, "Donnerstag") != 0 
    && strcmp(day, "Freitag") != 0 && strcmp(day, "Samstag") != 0 && strcmp(day, "Sonntag") != 0) {
        printf("Kein Wochentag!\n ");
        return;
    } else if(strcmp(day, "Montag") == 0) {
        printf("Der erste Wochentag\n");
    } else if(strcmp(day, "Dienstag") == 0) {
        printf("Der zweite Wochentag\n");
    } else if(strcmp(day, "Mittwoch") == 0) {
        printf("Der dritte Wochentag\n");
    } else if(strcmp(day, "Donnerstag") == 0) {
        printf("Der vierte Wochentag\n");
    } else if(strcmp(day, "Freitag") == 0) {
        printf("Der fünfte Wochentag\n");
    } else if(strcmp(day, "Samstag") == 0) {
        printf("Der sechste Wochentag\n");
    } else if(strcmp(day, "Sonntag") == 0) {
        printf("Der siebte Wochentag\n");
    }
}

void aufg5(void) {
    char gesch;
    printf("Geben Sie das Geschlecht ein:\n ");
    scanf("%s",&gesch);
    switch(gesch) {
        case 'm':
            printf("männlich\n");
            break;
        case 'M':
            printf("männlich\n");
            break;
        case 'w':
            printf("weiblich\n");
            break;
        case 'W':
            printf("weiblich\n");
            break;
        default:
            printf("Nicht definiert\n");
    } // switch
}

void aufg6(void) {
    srand(time(NULL));
    int n1 = rand(), n2 = rand(), n3 = rand();
    printf("%d %d %d\n", n1, n2, n3);
    if(n1 > n2 && n1 > n3) {
        printf("biggest %d\n", n1);
    } else if(n2 > n1 && n2 > n3) {
        printf("biggest %d\n", n2);
    } else if(n3 > n1 && n3 > n2) {
        printf("biggest %d\n", n3);
    }
    if(n1 < n2 && n1 < n3) {
        printf("smallest %d\n", n1);
    } else if(n2 < n1 && n2 < n3) {
        printf("smallest %d\n", n2);
    } else if(n3 < n1 && n3 < n2) {
        printf("smallest %d\n", n3);
    }
}

int aufg7(int num) {
    int first = num%10;
    int second = num/10;
    return first*10+second;
}

void aufg8(int *val) {
    ++(*val);
}

void aufg9(int val) {
    aNum += val;
}

bool aufg10(char* str) {
    return strcmp(str, "Cprogramming") ? false : true;
}

void aufg11(int age) {
    printf("Sie fürfen wählen %s\n", age >= 18 ? "ja" : "nein");
}

void aufg12(int mark) {
    switch(mark) {
        case 6:
            printf("Super\n");
            break;
        case 5:
            printf("Gut\n");
            break;
        case 4:
            printf("Genügend\n");
            break;
        case 3:
            printf("Ungenügend\n");
            break;
        case 2:
            printf("Schlecht\n");
            break;
        case 1:
            printf("Erbärmlich\n");
            break;
        default:
            printf("Falsche eingabe\n");
    } // switch
}