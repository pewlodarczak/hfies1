#include <stdio.h>
#include <string.h>
#include <time.h>
 
void getSeconds(long long *par);

int main () {

    long long sec;
    long long *pSec = &sec;
    getSeconds(pSec);

    /* print the actual value */
    printf("Number of seconds: %ld\n", sec );
    char * time_str = ctime(&sec);
    time_str[strlen(time_str)-1] = '\0';
    printf("Current Time : %s\n", time_str);
    return 0;
}

void getSeconds(long long *par) {
   /* get the current number of seconds */
   *par = time(NULL);
   //printf("Number of seconds: %ld\n", *par );
   return;
}