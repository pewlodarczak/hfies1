#ifndef AUFG_2
#define AUFG_2

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

void aufg1(void);
void aufg2(void);
void aufg3(void);
void aufg4(void);
void aufg5(void);
void aufg6(void);
int aufg7(int num);
void aufg8(int *val);
void aufg9(int val);
bool aufg10(char* str);
void aufg11(int age);
void aufg12(int mark);

#endif