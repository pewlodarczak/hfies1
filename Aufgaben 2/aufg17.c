#include <stdio.h>
#include <strings.h>

int main(int argc, char **argv) {
    printf("you entered in reverse order:\n");
    int numArgs = argc;
    while(argc--) {
        printf("%s\n",argv[argc]);
    }
    //printf("\n\n");
    char *args[numArgs-1];
    for(int i = 0; i < numArgs-1; i++) {
        //printf("%s\n",argv[i]);
        args[i] = argv[i+1];
        //printf("%s\n",args[i]);
    }
    printf("\n");
    for(int i = 0; i < numArgs-1; i++) {
        printf("%s\n",args[i]);
    }

    return 0;
}