#include <stdio.h>
#include<string.h>

int main() {
    int i = 1;
    char *sports[] = {
                         "swimming",
                         "hockey",
                         "football",
                         "wrestling",
                         "fencing"
                     };

    for(i = 0; i < 5; i++) {
        printf("String = %10s", *(sports+i));
        printf("\tAddress of string literal = %p\n", sports[i]);
    }

    // for(i = 0; i < 5; i++) {
    //     for(int j = 0; j < 10; j++) {
    //         printf("%c", sports[i][j]);
    //     }
    //     printf("\n");
    // }

    // signal to operating system program ran fine
    return 0;
}