#include <stdio.h>

#define MAX_LENGTH 20

void str_cpy(char *new, char *old);

int main() {
    char strOrig[MAX_LENGTH] = "Hello C";
    char str[1];
    str_cpy(str, strOrig);
    printf("\nnew str %s", str);

    str_cpy(str, "Hello C Programming");
    printf("\nnew str: %s", str);
}

void str_cpy(char *new, char *old) {
    int i = 0;
    while(*(old+i) != '\0') {
        i++;
    }
    printf("\narr size: %d\n", i);
    new[i+1];
    int j = 0;
    for(j = 0; j < i; j++) {
        *(new+j) = *(old+j);
    }
    new[j] = '\0';
}
