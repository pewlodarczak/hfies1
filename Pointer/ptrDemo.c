#include <stdio.h>

void incre(int v);
void increp(int *pV);

int main() {
    int val = 17;
    int *pVal;
    pVal = &val;
    //printf("%d\n", *pVal);
    printf("%d\n", val);
    incre(val);
    printf("%d\n", val);
    increp(pVal);
    printf("%d", val);
}

// Pass by value
void incre(int v) {
    ++v;
    printf("%d\n", v);
}

// Pass by reference
void increp(int *pV) {
    ++(*pV);
    printf("%d\n", *pV);
}