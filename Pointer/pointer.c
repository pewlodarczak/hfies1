#include <stdio.h>

void incr(int *value);

int main() {
    int val = 10;
    int *pVal = &val;
    printf("Val %d\n", val);
    incr(&val);
    printf("Val %d\n", val);

    printf("Val %d\n", *pVal);
    printf("Address of pVal %p\n", &pVal);
    printf("Address stored in %p\n", pVal);
    printf("Address of val %p\n", &val);
}

void incr(int *value) {
    ++(*value);
    printf("Val in incr() %d\n", *value);
}