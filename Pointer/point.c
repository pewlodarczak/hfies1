#include <stdio.h>

void incByValue(int aVal);
void incByRef(int *aVal);

int main() {
    int val = 9;
    int *pVal = &val;
    incByValue(val);
    printf("%d\n", val);
    incByRef(&val);
    printf("%d\n", val);
    // printf("%p\n", &val);
    // printf("%p\n", pVal);
    printf("%d\n", *pVal);
}

void incByRef(int *aVal) {
    *aVal += 2;
}

void incByValue(int aVal) {
    aVal += 2;
    printf("%d\n", aVal);
}
