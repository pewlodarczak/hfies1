#include <stdio.h>

void multDouble(double *dArr[]);

int main() {
    int i = 0;
    double dArr[] = {234.34, 567.32, 43.345};
    for(i = 0; i < 3; i++) {
        printf("%10.2lf\n", dArr[i]);
    }
    double *pdArr[3];
    for(i = 0; i < 3; i++) {
        pdArr[i] = &dArr[i];
    }
    printf("\n");
    for(i = 0; i < 3; i++) {
        printf("%10.2lf\n", *pdArr[i]);
    }

    multDouble(pdArr);
    printf("\n");
    for(i = 0; i < 3; i++) {
        printf("%10.2lf\n", *pdArr[i]);
    }
}

void multDouble(double *dArr[]) {
    for(int i = 0; i < 3; i++) {
        *dArr[i] *=  10;
    }
}