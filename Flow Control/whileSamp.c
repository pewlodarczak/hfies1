#include <stdio.h>
#include <stdbool.h>

int main()
{
    bool running = true;
    printf("Let the games begin:\n");
    while (running)
    {
        printf("1\tDoom Eternal\n");
        printf("2\tDestiny 2\n");
        printf("3\tCall of Duty\n");
        printf("4\tExit\n");

        int sel = 0;
        scanf("%d", &sel);
        switch (sel)
        {
        case 1:
            printf("Doom Eternal\n");
            break;
        case 2:
            printf("Destiny 2\n");
            break;
        case 3:
            printf("Call of Duty\n");
            break;
        case 4:
            printf("Byebye\n");
            running = false;
            break;
        default:
            printf("Error! Game doesn't exist\n\n");
        } // switch
    }     // while
    printf("Out of loop \n");
}