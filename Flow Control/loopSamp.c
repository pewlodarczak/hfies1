#include <stdio.h>

int main() {
    int i = 0;
    for(; i < 3; i++) {
        printf("Iter %d\n", i);
    } // for
    printf("Out of loop %d\n", i);

    for(i = 0; i < 3; i++) {
        printf("Iter %d\n", i);
    } // for
}