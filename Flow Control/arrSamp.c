#include<stdio.h>

int main() {
    float iarr[] = {1.56, 5.8, 9, 13.2, 2, 1};
    //int iarr[3];
    //iarr[1] = 7;
    for(int i = 0; i < sizeof(iarr)/sizeof(float); i++) {
        printf("Val%d %.2f\n", i, iarr[i]);
    }

    char cArr[] = {'a', 'b', 'g', 'z'};
    for(int i = sizeof(cArr)/sizeof(char) - 1; i >= 0 ; i--) {
        printf("Char%d %c\n", i, cArr[i]);
    }
    printf("\n");
    int i = 0;
    while(i < sizeof(cArr)/sizeof(char)) {
        printf("Char%d %c\n", i, cArr[i]);
        i++;
    }
    printf("\n");
    i = 0;
    do{
        printf("Char%d %c\n", i, cArr[i]);
        i++;
    } while(i < sizeof(cArr)/sizeof(char));

}