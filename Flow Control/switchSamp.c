#include<stdio.h>

void dayOfWeek(char c);

int main() {
    char chr = 'u';
    printf("Enter day:\n");
    scanf("%c", &chr);
    dayOfWeek(chr);
}

void dayOfWeek(char c) {
        switch(c) {
        case 'm':
            printf("Monday\n");
            break;
        case 't':
            printf("Tuesday\n");
            break;
        case 'w':
            printf("Wednesday\n");
            break;
        case 'h':
            printf("Thursday\n");
            break;
        case 'f':
            printf("Friday\n");
            break;
        case 's':
            printf("Saturday\n");
            break;
        case 'u':
            printf("Sunday\n");
            break;
        default:
            printf("Error\n");
            break;
    } // switch

} // dayOfWeek