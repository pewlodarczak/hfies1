#include <stdio.h>

float CelsiusToFahrenheit(float celsius);
float CelsiusToKelvin(float celsius);
void menu(float celsius);

int main() {
    printf("Geben Sie die Temperatur in Celsius ein:\n");
    float celsius;
    char c;
    scanf("%f", &celsius);
    menu(celsius);
    //CelsiusToFahrenheit(celsius);
    //float kel = CelsiusToKelvin(celsius);
    //printf("Fahrenheit %.2f\n", CelsiusToFahrenheit(celsius));
    //printf("Kelvin %.2f\n", CelsiusToKelvin(celsius));
}

void menu(float celsius) {

    printf("In was soll %.2f konvertiert werden?\n", celsius);
    printf("f\tFür Fahrenheit\n");
    printf("k\tFür Kelvin\n");
    char c;
    scanf(" %c", &c);
    switch(c) {
        case 'f':
            printf("%.2f Celsius ist %.2f Fahrenheit\n", celsius, CelsiusToFahrenheit(celsius));
            break;
        case 'k':
            printf("%.2f Celsius ist %.2f Kelvin\n", celsius, CelsiusToKelvin(celsius));
            break;
        default:
            printf("Ungültige Eingabe\n");
    }

}

float CelsiusToFahrenheit(float celsius) {
    // Fahrenheit = Celsius * 1.8 + 32
    printf("Fahrenheit %.2f\n", celsius * 1.8 + 32);
    return celsius * 1.8 + 32;
}

float CelsiusToKelvin(float celsius) {
    // Kelvin = Celsius + 273.15
    printf("Kelvin %.2f\n", celsius + 273.15);
    return celsius + 273.15;
}