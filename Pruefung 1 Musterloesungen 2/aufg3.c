#include<stdio.h>

void threeNumbers(void);
int diff(void);
float mean();
void evenOdd();

int a, b, c;

int main() {
    threeNumbers();
    printf("Diff %d\n", diff());
    printf("Mean %f\n", mean());
    evenOdd();
}

void threeNumbers() {
    printf("Geben Sie 3 ganze Zahlen ein\n");
    scanf("%d %d %d", &a, &b, &c);
    printf("Die Zahlen: %d %d %d\n", a, b, c);
}

int diff() {
    int biggest, smallest;
    if(a > b && a > c) {
        biggest = a;
    } else if(b > a && b > c) {
        biggest = b;
    } else
        biggest = c;

    printf("Biggest %d\n", biggest);

    if(a < b && a < c) {
        smallest = a;
    } else if(b < a && b < c) {
        smallest = b;
    } else {
        smallest = c;
    }

    printf("Smallest %d\n", smallest);
    return biggest - smallest;
}

float mean() {
    return (float)(a + b + c) / 3;
}

void evenOdd() {
    int e = 0, o = 0;
    if(a%2) {
        o++;
    } else {
        e++;
    }
    if(b%2) {
        o++;
    } else {
        e++;
    }
    if(c%2) {
        o++;
    } else {
        e++;
    }
    printf("Gerade: %d ungerade: %d\n", e, o);
}