#include <stdio.h>

void ageCheck(int age);

int main() {
    printf("Geben Sie ihr Alter ein:\n");
    int age;
    scanf("%d",&age);
    ageCheck(age);
}

void ageCheck(int age) {
    if(age < 16 && age > 0) {
        printf("No alcohol\n");
    } else if (age >= 16 && age < 18) {
        printf("Beer\n");
    } else if (age >= 18) {
        printf("Everything\n");
    } else {
        printf("Error\n");
    }
}