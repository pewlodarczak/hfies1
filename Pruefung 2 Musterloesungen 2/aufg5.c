#include <stdio.h>

void sum(int *a, int *b);
void sum2(int *a, int *b, int * res);

int main() {
    int a = 5, b = 7;
    int *pA = &a, *pB = &b;
    sum(pA, pB);
    printf("Sum: %d", *pA);

    int res;
    sum2(pA, pB, &res);
    printf("\nSum: %d", res);
}

void sum(int *a, int *b) {
    *a += *b;
}

// oder
void sum2(int *a, int *b, int * res) {
    *res = *a + *b;
}
