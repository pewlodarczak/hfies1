#include <stdio.h>

void create2DArr();

int main() {
    create2DArr();
}

void create2DArr() {
    //char arr[][8] = {{'(', 'o', ')', '/', '\\', '(', 'o', ')'}, {' ', ' ', '-', '-', '-', '-', ' ', ' '}};
    char arr[][7] = {{'(', '\\', '_', '_', '_', '/', ')'}, {'(', '=', '^', '.', '^', '=', ')'}, {'(', '"', ')', '_', '(', '"', ')'}};
    printf("\n\n");
    for(int r = 0; r < 3; r++) {
        printf("\t");
        for(int c = 0; c < 7; c++) {
            printf("%c", arr[r][c]);
        }
        printf("\n");
    }
    printf("\n\n");
}