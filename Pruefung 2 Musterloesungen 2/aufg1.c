#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define SIZE 10

void initializeArray();
void biggestNum();
void evenOdd();
void incr();

int arr[SIZE];

int main() {
    initializeArray();
    for(int i = 0; i < SIZE; i++) {
        printf("%d\n",arr[i]);
    }
    biggestNum();
    evenOdd();
    incr();
    for(int i = 0; i < SIZE; i++) {
        printf("%d\n",arr[i]);
    }

}

// a
void initializeArray() {
    srand(time(NULL));
    for(int i = 0; i < SIZE; i++) {
        arr[i] =  rand() % 20;
    }
}

// b
void biggestNum() {
    int biggest = 0;
    for(int i = 0; i < SIZE; i++) {
        if(arr[i] > biggest)
            biggest = arr[i];
    }
    printf("\nbiggest num %d\n", biggest);
}

//c
void evenOdd() {
    int even = 0, odd = 0;
    for(int i = 0; i < SIZE; i++) {
        if(arr[i]%2 == 0)
            even++;
        else
            odd++;
    }
    printf("\neven %d odd %d\n", even, odd);
}

// d
void incr() {
    for(int i = 0; i < SIZE; i++) {
        arr[i] += 2;
    }
}