#include <stdio.h>
#include <string.h>

int main() {
    char str[] = "Ich weiss, dass ich nichts weiss."; // Sokrates (469 - 399 v. Chr.)
    int numWords = 0;
    for(int i = 0; i < strlen(str); i++) {
        if(str[i] == ' ' || str[i] == '.')
            numWords++;
    }
    printf("Number of words: %d", numWords);
}