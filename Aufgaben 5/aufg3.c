#include <stdio.h>
#include <time.h>

#define MAX_SIZE 10

int* getArr();

int main() {
    for(int i = 0; i < MAX_SIZE; i++) {
        printf("Num: %d\n", *(getArr()+i));
    }
}

int* getArr() {
    static int arr[MAX_SIZE];
    srand(time(NULL));
    for(int i = 0; i < MAX_SIZE; i++) {
        *(arr+i) = 10 + rand() % 11; // zwischen 10 und 20
    }
    return arr;
}