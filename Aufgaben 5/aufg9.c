#include <stdio.h>

#define MAX_SIZE 100

void strCat(char *str1, char *str2);

int main() {
    char s1[MAX_SIZE] = "Hello", s2[MAX_SIZE] = "World";
    // char *str1 = s1;
    // char *str2 = s2;
    strCat(s1, s2);
    printf("Concatenated: %s", s1);
}

void strCat(char *str1, char *str2) {
    while(*(++str1));
    while(*(str1++) = *(str2++));
}