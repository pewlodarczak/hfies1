#include <stdio.h>
#include <string.h>

char* strCat(char *arg1, char *arg2);

int main(int argc, char **argv) {
    printf("you entered:\n");
    for(int i = 1; i < argc; i++) {
        printf("%s\n", argv[i]);
    }
    if(argv[1] == NULL)
        return -1;
    //strCat(argv[1], argv[2]);
    // strcat(argv[1], argv[2]);
    // printf("Concatenated: %s\n", argv[1]);

    //printf("Concatenated: %s\n", strCat(argv[1], argv[2]));
    int i = 1;
    while (i < argc) {
        strcat(argv[1], argv[i]);
        i++;
    }
    printf("Concatenated: %s\n", argv[1]);
    return 0;
}

char* strCat(char *arg1, char *arg2) {
    static char newStr[100];
    int i = 0, j = strlen(arg1);
    for(; i < j; i++) {
        newStr[i] = *(arg1++);
    }
    int catStr = i + strlen(arg2);
    for(; i <  catStr; i++) {
        newStr[i] = *(arg2++);
    }
    return newStr;
}