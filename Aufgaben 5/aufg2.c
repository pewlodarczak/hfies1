#include <stdio.h>
#include <time.h>

#define MAX_SIZE 100

void gettime(int *time);
void getcurrenttime(char *tme);

int main() {
    int time;
    gettime(&time);
    printf("Seconds since January 1, 1970 = %ld\n", time);
    char currtime[50];
    getcurrenttime(currtime);
    printf("strlen %d\n", strlen(currtime));
    printf("Current time = %s\n", currtime);
}

void gettime(int *timeinsec) {
    *timeinsec = time(NULL);
}

void getcurrenttime(char *tme) {
    time_t t;
    time(&t);
    char *currtme = ctime(&t);
    while(*(tme++) = *(currtme++));
}