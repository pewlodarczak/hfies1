#include <stdio.h>

void changeVar(char *aPtr);
void prntStr(char *aStr);
void strCpy(char *str1, char *str2);

int main() {
    char var = 'a';
    char* pVar = &var;

    printf("%c\n", var); // Wert
    printf("%p\n", &var); // Adresse
    printf("%p\n", pVar); // Adresse von var
    printf("%c\n", *pVar); // Wert von var

    changeVar(pVar);
    printf("%c\n", var); // Wert

    // string
    char str[20] = "hello";
    //printf("%s\n", str); // String
    prntStr(str);
    printf("\n");
    char str2[20] = "C Programming";

    strCpy(str, str2);
    prntStr(str);
}

void changeVar(char *aPtr) { // pass by reference
    *aPtr = 'z';
}

void prntStr(char *aStr) {
    while(*aStr != '\0')
        printf("%c", *(aStr++));
}

void strCpy(char *str1, char *str2) {
    while(*(str1++) = *(str2++));
}