#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define NUMBER_OF_PARTIC 50
#define MAX_STRING_SIZE 40

int main() {
    char participants[NUMBER_OF_PARTIC][MAX_STRING_SIZE];
    printf("Enter the participants:\n");
    printf("q to quit\n");
    int r = 0;
    while(r < NUMBER_OF_PARTIC) {
        char name[MAX_STRING_SIZE];
        gets(name);
        if (strcmp(name, "q") == 0) {
            printf("Bye\n");
            participants[r][0] = NULL;
            break;
        }

        strcpy(participants[r], name);
        printf("Partic: %s\n", participants[r]);
        r++;
    } // while

    r = 0;
    while(*participants[r]) {
        printf("Partic: %s\n", participants[r]);
        r++;
    } // while

}