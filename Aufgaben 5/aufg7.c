#include <stdio.h>

int arrSize(char *str);

int main() {
    char *str = "C Programming";
    printf("String %s length %d", str, arrSize(str));
}

int arrSize(char *str) {
    int len = 0;
    while(*(str++) != '\0')
        len++;
    return len;
}