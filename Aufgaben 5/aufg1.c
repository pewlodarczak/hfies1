#include <stdio.h>
#include <string.h>

#define MAX_SIZE 100

void strCpy(char *str1, char *str2);
void strCpyEx(char *str1, char *str2);

int main() {
    char s1[MAX_SIZE] = "Hello World", s2[MAX_SIZE] = "C Programming";
    printf("Orig: %s\n", s1);
    strCpy(s1, s2);
    printf("Copied: %s\n", s1);
    char *newStr;
    strCpyEx(newStr, s2);
    printf("\nNew arr: %s\n", newStr);
}

void strCpy(char *str1, char *str2) {
    while(*(str1++) = *(str2++));
}

void strCpyEx(char *str1, char *str2) {
    str1[strlen(str2) + 1];
    while(*(str1++) = *(str2++));
}