#include "aufg1.h"

int val = 8;
//const float PI = 3.14159;

int main() {
    //printf("Aufg 1: %.2f\n", aufg1(3, 7, 9));
    //printf("Aufg 2: %f\n", aufg2(135));
    //printf("Aufg 3: %.2f\n", aufg3(25.2));
    //printf("Aufg 4: %.2f\n", aufg4(40));
    //printf("Aufg 5: %f\n", aufg5(3, 7, 9));
    //printf("Aufg 6: %f\n", aufg6(9));
    //int i;
    //printf("Geben sie eine ganze Zahl ein\n");
    //scanf("%d", &i);
    //bool isEven = aufg7(i);
    //printf("Aufg 7: %d\n", aufg7(i));
    //float temp, hum;
    //printf("Wie hoch ist die Temperatur?\n");
    //scanf("%f", &temp);
    //printf("Wie hoch ist die Feuchtigkeit?\n");
    //scanf("%f", &hum);
    //printf("Aufg 8: %s\n", aufg8(temp, hum) ? "true" : "false");
    //printf("Aufg 9:");
    //aufg9(8, 11, 9);
    //printf("\n");
    //printf("Aufg 10: %c is vowle %s\n", 'f', aufg10('f') ? "true" : "false");
    //aufg11();
    //printf("Aufg 11: %c is vowle %s\n", 'f', aufg11());
    //printf("Geben Sie eine ganze Zahl ein:\n");
    //int num = 0;
    //scanf("%d", &num);
    //printf("Die Zahl ist gerade %s\n", aufg12(num) ? "wahr" : "falsch");
    aufg13();
    //aufg15();
    // scopeDemo();
}

float aufg1(float val1, float val2, float val3) {
    return val1 * val2 * val3;
}

float aufg2(int min) {
    return min * sec;
}

float aufg3(float age) {
    return age * DAYS_IN_YEAR;
} 

float aufg4(float farenheit) {
    return (farenheit - 32) / 1.8;
}

float aufg5(float val1, float val2, float val3) {
    printf("val %d\n", val);
    return (val1 + val2 + val3)/3;
}

float aufg6(float radius) {
    return radius * radius * PI;
}

bool aufg7(int num) {
    if(num%2 == 0) {
        return true;
    } else {
        return false;
    }
}

bool aufg8(float temp, float hum) {
    return temp > 30 && hum > 60 ? false : true;
}

void aufg9(float val1, float val2, float val3) {
    if(val1 > val2 && val1 > val3)
        printf("Biggest of %.2f %.2f %.2f in %.2f\n", val1, val2, val3, val1);
    else if(val2 > val1 && val2 > val3)
        printf("Biggest of %.2f %.2f %.2f in %.2f\n", val1, val2, val3, val2);
    else
        printf("Biggest of %.2f %.2f %.2f in %.2f\n", val1, val2, val3, val3);
}

bool aufg10(char chr) {
    if(chr == 'a' || chr == 'e' || chr == 'i' || chr == 'o' || chr == 'u' ||
    chr == 'A' || chr == 'E' || chr == 'I' || chr == 'O' || chr == 'U')
        return true;
    else
        return false;
}

void aufg11(void) {
    bool running = true;
    do {
        printf("1\tDoom Eternal\n");
        printf("2\tDestiny 2\n");
        printf("3\tCall of Duty\n");
        printf("4\tExit\n");

        int sel = 0;
        scanf("%d", &sel);

        switch (sel)
        {
        case 1:
            printf("Doom Eternal\n");
            break;
        case 2:
            printf("Destiny 2\n");
            break;
        case 3:
            printf("Call of Duty\n");
            break;
        case 4:
            printf("Byebye\n");
            running = false;
            break;
        default:
            printf("Error! Game doesn't exist\n\n");
        } // switch     
    } while(running); // do
}

bool aufg12(int num) {
    return num%2? false : true;
}

bool aufg13(void) {
    printf("Sind Sie in einer Festanstellung? ja / nein\n");
    char str[10];
    scanf("%s", &str);
    if(strcmp(str,"ja") != 0 && strcmp(str,"nein") != 0) {
        printf("Geben Sie ja oder nein ein\n");
        return false;
    } else if(strcmp(str,"nein") == 0) {
        printf("Sorry keine Hypothek\n");
        return false;
    } else {
        printf("Verdienen Sie mehr als 100000.-? ja / nein\n");
        scanf("%s", &str);
        if(strcmp(str,"nein") == 0) {
            printf("Sorry keine Hypothek\n");
            return false;
        } else {
            printf("Haben Sie ein Vermögen von mehr als 250000.-? ja / nein\n");
            scanf("%s", &str);
            if(strcmp(str,"nein") == 0) {
                printf("Sorry keine Hypothek\n");
                return false;
            } else {
                printf("Sie bekommen eine Hypothek\n");
                return true;
            }
        }
    }
}

void aufg15(void) {
    char operation;
    double n1, n2;

    printf("Enter an operator (+, -, *, /): ");
    scanf("%c", &operation);
    printf("Enter two operands: ");
    scanf("%lf %lf",&n1, &n2);

    switch(operation)
    {
        case '+':
            printf("%.1lf + %.1lf = %.1lf",n1, n2, n1+n2);
            break;

        case '-':
            printf("%.1lf - %.1lf = %.1lf",n1, n2, n1-n2);
            break;

        case '*':
            printf("%.1lf * %.1lf = %.1lf",n1, n2, n1*n2);
            break;

        case '/':
            printf("%.1lf / %.1lf = %.1lf",n1, n2, n1/n2);
            break;

        // operator doesn't match any case constant +, -, *, /
        default:
            printf("Error! operator is not correct");
    }
}

void scopeDemo() {
    printf("val %d\n", val);
    ++val;
    printf("val %d\n", val);
}