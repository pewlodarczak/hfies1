#ifndef AUFG_1
#define AUFG_1

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define sec 60
#define PI 3.14159
#define DAYS_IN_YEAR 365

float aufg1(float val1, float val2, float val3);
float aufg2(int min);
float aufg3(float age);
float aufg4(float farenheit);
float aufg5(float val1, float val2, float val3);
float aufg6(float radius);
bool aufg7(int num);
bool aufg8(float temp, float hum);
void aufg9(float val1, float val2, float val3);
bool aufg10(char chr);
void aufg11(void);
bool aufg12(int num);
bool aufg13(void);
void aufg15(void);

void scopeDemo();

#endif