#include "aufg2.h"

int main() {
    //aufg7();
    //aufg8(20, 70);
    aufg9();
    //aufg10();
    aufg11();
    
    if(aufg8(20, 50)) {
        printf("play golf\n");
    } else {
        printf("don't play golf\n");
    }
}

void aufg7() {
    int num;
    printf("Enter a number:\n");
    scanf("%d", &num);

    if (num%2 == 0) {
        printf("%d is even\n", num);
    } else {
        printf("%d is odd\n", num);
    }
}

bool aufg8(float temp, float humi) {
    if (temp > 30 && humi > 60) {
        return false;
    } else {
        return true;
    }
}

void aufg9() {
    int num1, num2, num3;
    printf("Enter 3 numbers:\n");
    scanf("%d %d %d", &num1, &num2, &num3);
    if(num1 > num2 && num1 > num3) {
        printf("%d is biggest\n", num1);
    } else if(num2 > num1 && num2 > num3) {
        printf("%d is biggest\n", num2);
    } else
        printf("%d is biggest\n", num3);
}

void aufg10() {
    char aChar;
    printf("Enter a character:\n");
    scanf("%d", &aChar);

    if(aChar == 'a' || aChar == 'e' || aChar == 'i' || aChar == 'o' || aChar == 'u' 
    || aChar == 'A' || aChar == 'E' || aChar == 'I' || aChar == 'O' || aChar == 'U' ) {
        printf("Is vowel\n");
    } else{
        printf("No vowel\n");
    }
}

void aufg11() {
    bool running = true;
    int sel;
    printf("Let the games begin:\n");
    do {
        printf("1\tDoom Eternal\n");
        printf("2\tDestiny 2\n");
        printf("3\tCall of Duty\n");
        printf("4\tExit\n");
        printf("What do you want to play?\n");
        scanf("%d", &sel);
        if(sel == 1) {
            printf("Doom Eternal\n");
        } else if(sel == 2) {
            printf("Destiny 2\n");
        } else if(sel == 3) {
            printf("Call of Duty\n");
        } else if(sel == 4) {
            printf("Byebye\n");
            running = false;
        } else
            printf("Error! Game doesn't exist\n");
    } while (running);
}