#ifndef AUFG2_H
#define AUFG2_H

#include <stdio.h>
#include <stdbool.h>

void aufg7(void);
bool aufg8(float temp, float humi);
void aufg9(void);
void aufg10(void);
void aufg11(void);

#endif