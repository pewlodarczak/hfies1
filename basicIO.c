#include <stdio.h>

int main() {
    int age;
    printf("Enter your age:\n");
    scanf("%d", &age);
    if(age >= 16 && age < 18) {
        printf("Your can buy beer");
    } else if (age >= 18) {
        printf("You can buy whiskey\n");
    } else {
        printf("You are too young");
    }
}