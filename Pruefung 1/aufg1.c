#include <stdio.h>

void checkAge(int age);

int main() {
    printf("Geben Sie ihr Alter ein:\n");
    int age;
    scanf("%d",&age);
    checkAge(age);
}

void checkAge(int age) {
    if(age <= 0) {
        printf("Ungültige eingabe\n");
        return;
    }
    if(age < 16) {
        printf("Sorry kein Alkohol\n");
        return;
    } else if(age >= 16 && age < 18)
        printf("Bier und Wein\n");
    else
        printf("Jeder Alkohol\n");
}