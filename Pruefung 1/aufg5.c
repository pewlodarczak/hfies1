#include <stdio.h>
#include <stdbool.h>

void incr(int *num);

int main() {
    int num = 10;
    int* pNum = &num;
    incr(pNum);
    printf("Erhöht: %d\n", *pNum);
}

void incr(int* num) {
    *num+=2;
}