#include <stdio.h>

int differ(int f1, int f2, int f3);
float mean(int f1, int f2, int f3);
void evenOdd(int f1, int f2, int f3);

int main() {
    printf("Geben Sie 3 Zahlen ein:\n");
    int f1, f2, f3;
    scanf("%d %d %d", &f1, &f2, &f3);
    printf("Die Differenz zwischen der grössten und kleinsten Zahl ist: %d\n", differ(f1, f2, f3));
    printf("Mittelwert: %.2f\n", mean(f1, f2, f3));
    evenOdd(f1, f2, f3);
}

int differ(int n1, int n2, int n3) {
    int biggest, smallest;
    if(n1 > n2 && n1 > n3) {
        printf("biggest %d\n", n1);
        biggest = n1;
    } else if(n2 > n1 && n2 > n3) {
        printf("biggest %d\n", n2);
        biggest = n2;
    } else if(n3 > n1 && n3 > n2) {
        printf("biggest %d\n", n3);
        biggest = n3;
    }
    if(n1 < n2 && n1 < n3) {
        printf("smallest %d\n", n1);
        smallest = n1;
    } else if(n2 < n1 && n2 < n3) {
        printf("smallest %d\n", n2);
        smallest = n2;
    } else if(n3 < n1 && n3 < n2) {
        printf("smallest %d\n", n3);
        smallest = n3;
    }
    return biggest - smallest;
}

float mean(int f1, int f2, int f3) {
    return (float)(f1 + f2 + f3) /3;
}

void evenOdd(int f1, int f2, int f3) {
    int e = 0, o = 0;
    if(f1%2 == 0) {
        e++;
    } else {
        o++;
    }
    if(f2%2 == 0) {
        e++;
    } else {
        o++;
    }
    if(f3%2 == 0) {
        e++;
    } else {
        o++;
    }
    printf("%d gerade, %d ungerade\n", e, o);
}