#include <stdio.h>
#include <stdbool.h>

bool leapYear(int year);

int main() {
    int year;
    printf("Geben Sie ein Jahr ein:\n");
    scanf("%d", &year);
    printf("%d ist ein Schaltjahr: %s\n", year, leapYear(year) ? "wahr" : "falsch");
}

bool leapYear(int year) {
    return year%4 == 0;
}