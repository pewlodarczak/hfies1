#include <stdio.h>

float celsiusToF(float degree);
float celsiusToK(float degree);

int main() {
    printf("Geben Sie die Temperatur in Celsius ein:\n");
    float temp;
    scanf("%f", &temp);
    printf("In was soll %f° konvertiert werden?\n", temp);
    printf("f\tFür Fahrenheit\n");
    printf("k\tFür Kelvin\n");
    char c;
    scanf(" %c", &c);
    switch(c) {
        case 'f':
            printf("%.2f Celsius ist %.2f Fahrenheit\n", temp, celsiusToF(temp));
            break;
        case 'k':
            printf("%.2f Celsius ist %.2f Kelvin\n", temp, celsiusToK(temp));
            break;
        default:
            printf("Ungültige Eingabe\n");
    }

    
}

float celsiusToF(float degree) {
    return degree*1.8 + 32;
}

float celsiusToK(float degree) {
    return degree + 273.15;
}