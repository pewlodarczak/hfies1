#include <stdio.h>

int main() {
    char *str = "hello";
    int i = 0;
    for(; i < sizeof(str)/sizeof(char)-2; i++) {
        printf("%c", str[i]);
    }
    printf("\n");
    i = 0;
    while(*(str+i) != '\0') {
        printf("%c", *(str+i));
        i++;
    }
    printf("\ni %d", i);
}