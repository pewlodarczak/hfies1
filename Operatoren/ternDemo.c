#include <stdio.h>

char* threshold(int i);

int main() {
    int i = 0;
    printf("Welche Grenzwert?\n");
    scanf("%d", &i);
    printf("Above %s", threshold(i));
}

char* threshold(int i) {
    int thresh = 20;
    return i > thresh ? "true" : "false";
}