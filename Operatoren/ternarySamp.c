#include <stdio.h>
#include <stdbool.h>

bool isEven(int i);

int main() {
    int val = 0;
    printf("Geben Sie einen Wert ein\n");
    scanf("%d", &val);
    printf("Is even: %d", isEven(val));
}

bool isEven(int i) {
    return i%2 == 0 ? true : false; 
}