#include <stdio.h>

void printMenu();

int main() {
    printMenu();
}

void printMenu() {
    printf("Was ist Ihr Level:\n");
    printf("a\tFür Anfänger\n");
    printf("f\tFür Fortgeschritten\n");
    printf("e\tFür Experte\n");
    char chr;
    scanf("%c", &chr);
    switch(chr) {
        case 'a':
            printf("Ihr Level: Anfänger\n");
            break;
        case 'f':
            printf("Ihr Level: Fortgeschritten\n");
            break;
        case 'e':
            printf("Ihr Level: Experte\n");
            break;
        default:
            printf("Ungültige Eingabe\n");
    }
}