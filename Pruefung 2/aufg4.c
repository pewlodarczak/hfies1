#include <stdio.h>

int main() {
    printf("Enter a number:\n");
    long long int num;
    scanf("%lld", &num);
    printf("Number = %lld", num);
    int digits = 0;
    while(num > 0) {
        num /= 10;
        digits++;
    }
    printf("\nDigits = %d", digits);
}
