#include <stdio.h>

long long int processNumbers(int a, int b);

int main() {
    printf("Enter two numbers:\n");
    int num1, num2;
    scanf("%d %d", &num1, &num2);
    printf("Faculty = %lld", processNumbers(num1, num2));
}

long long int processNumbers(int a, int b) {
    long int bigger = 0;
    if(a > b) {
        printf("%d bigger than %d\n", a, b);
        bigger = (long long int)a;
    } else if(a == b) {
        printf("%d equal to %d\n", a, b);
        bigger = (long long int)a;
    } else {
        printf("%d bigger than %d\n", b, a);
        bigger = (long long int)b;
    }
    printf("bigger %lld\n", bigger);
    long long int fac = 1;
    for(long long int i = 1; i <= bigger; i++) {
        fac *= i;
    }
    printf("Faculty = %lld\n", fac);
    return fac;
}