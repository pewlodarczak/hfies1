#include <stdio.h>

void incr(int *pV);

int main() {
    int val = 10;
    int *pV = &val;
    printf("Val: %d\n", val);
    incr(pV);
    printf("Val: %d\n", val);
}

void incr(int *pV) {
    *pV += 2;
}