#include <stdio.h>

void printHorizontally(int num);
void printVertically(int num);

int main() {
    printf("Enter number of hashes:\n");
    int num;
    scanf("%d", &num);
    printHorizontally(num);
    printVertically(num);
}

void printHorizontally(int num) {
    printf("\n");
    for(int i = 0; i < num; i++) {
        printf("#");
    }
}

void printVertically(int num) {
    printf("\n");
    for(int i = 0; i < num; i++) {
        printf("\t#\n");
    }
}