#include <stdio.h>

int main() {
    int level;
    do {
        printf("Select your level:\n");
        printf("1\tBeginner\n");
        printf("2\tIntermediate\n");
        printf("3\tAdvanced\n");
        printf("4\tExit\n");
        scanf("%d", &level);
        switch(level) {
            case 1:
                printf("\nYour level\tBeginner\n");
                break;
            case 2:
                printf("\nYour level\tIntermediate\n");
                break;
            case 3:
                printf("\nYour level\tAdvanced\n");
                break;
        }
    } while(level < 4);
}