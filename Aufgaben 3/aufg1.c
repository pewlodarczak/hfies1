#include <stdio.h>
#include "aufg1.h"

int main() {
    int numbers;
    printf("Enter number of fibonacci numbers:\n");
    scanf("%d", &numbers);
    printf("Show %d fibonacci numbers\n", numbers);
    //forFibbonacci(numbers);
    whileFibbonacci(numbers);
}

void forFibbonacci(int numb) {
    if(numb == 0)
        return;
    if(numb == 1) {
        printf("0");
        return;
    }
        
    unsigned long long int i = 0, j = 1, k;
    k = i + j;
    printf("%d %d", i, j);
    for(int in = 3; in <= numb; in++) {
        printf(" %d", k);
        i = j;
        j = k;
        k = i + j;
    }
}

void whileFibbonacci(int numb) {
    if(numb == 0)
        return;
    if(numb == 1) {
        printf("0");
        return;
    }
        
    unsigned long long int i = 0, j = 1, k;
    k = i + j;
    printf("%d %d", i, j);
    int in = 3; 
    while(in <= numb) {
        printf(" %d", k);
        i = j;
        j = k;
        k = i + j;
        in++;
    }
}