#include <stdio.h>

int main() {
    char cA[4][3] = {{"xox"}, {"oxo"}, {"xox"}, {"oxo"}};
    for(int r = 0; r < 4; r++) {
        for(int c = 0; c < 3; c++) {
            printf("%c", cA[r][c]);
        }
        printf("\n");
    }
}