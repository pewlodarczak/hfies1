#include <stdio.h>

int main() {
    long int number, firstDigit, lastDigit;

    printf("Enter a positive integer:\n");
    scanf("%li", &number);
    printf("You entered: %li\n", number);

    lastDigit = number % 10;

    do
    {
        number = number / 10;
        printf("Number: %li  %i\n", number, number % 10);
        if(number % 10 != 0)
            firstDigit = number;
    } while(number != 0);
    
    printf("First digit: %i\n", firstDigit);
    printf("Last digit: %i\n", lastDigit);
}