#include <stdio.h>

float findBiggest(float num, float biggest);

int main() {
    float num, biggest = 0;
    printf("%d\n", num);
    printf("Enter some number, 0 to quit:\n");
    do {
        scanf("%f", &num);
        if(num == 0)
            break;
        biggest = findBiggest(num, biggest);
    } while(num != 0);
    printf("Biggest number: %.2f", biggest);
}

float findBiggest(float num, float biggest) {
    if(num > biggest) {
        biggest = num;
        return biggest;
    } else {
        return biggest;
    }
}