#include <stdio.h>
#include <stdbool.h>

int main() {

    int n, i, flag = false;
    printf("Enter a positive integer:\n");
    scanf("%d", &n);

    if (n == 0 || n == 1)
        flag = true;

    for (i = 2; i <= n / 2; ++i) {
        if (n % i == 0) {
            flag = true;
            break;
        }
    }

    printf("%d is a prime number %s.", n, flag ? "false" : "true");
    return 0;
}