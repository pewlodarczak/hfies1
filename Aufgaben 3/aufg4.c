#include <stdio.h>

int main() {
    long int num;
    printf("Enter a big number:\n");
    scanf("%d", &num);
    int i = 0;
    do {
        num = num/10;
        i++;
    } while(num > 0);
    printf("Number of digits: %d", i);
}