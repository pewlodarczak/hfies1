#include <stdio.h>

int main() {
    int num;
    printf("Enter a number:\n");
    scanf("%d", &num);
    int i = 1;
    for(; ; ) {
        if(i <= num) {
            printf("%d ", i);
            i++;
        } else {
            printf("%d ", num);
            num--;
            if(num == 0)
                break;
        }
    }    
}